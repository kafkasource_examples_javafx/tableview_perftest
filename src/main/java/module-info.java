module com.kafkasource.tableviewperftest {
    requires javafx.controls;
    requires javafx.fxml;
    requires java.logging;
    requires org.apache.commons.text;
    requires com.kafkasource.visualkafka.lib;
    
    opens com.kafkasource.tableviewperftest to javafx.fxml;
    exports com.kafkasource.tableviewperftest;
}
