/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.kafkasource.tableviewperftest;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;
import javafx.collections.ObservableList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import org.apache.commons.text.RandomStringGenerator;
/**
 *
 * @author Chad Preisler
 */
public class DataLoader implements Runnable {
    
    static final Logger LOGGER = Logger.getLogger(DataLoader.class.getName());
    
    private ObservableList<Message> messages;
    RandomStringGenerator generator;
    
    public DataLoader(ObservableList<Message> messages) {
        this.messages = messages;
        generator = new RandomStringGenerator.Builder().withinRange('a', 'z').build();
    }

    @Override
    public void run() {
        int counter = 0;
        var tempArray = new ArrayList<Message>();
        LOGGER.info("Loading messages....");
        while (true && counter < 400000) {
            if (Thread.interrupted()) {
                LOGGER.log(Level.INFO, "DataLoader interrupted");
                break;
            }
            var message = generateRecord();
            tempArray.add(message);
            if (counter % 50000 == 0) {
                LOGGER.info("Writing to UI!!!!!!!!!!!!!!!");
                writeToUi(tempArray);
                tempArray = new ArrayList<Message>();
            }
            counter++;
        }
        if (tempArray.size() > 0) {
            writeToUi(tempArray);
        }
    }
    
    private void writeToUi(ArrayList<Message> tempMessages) {
        Platform.runLater(() -> {
            LOGGER.log(Level.INFO, "tempMessages.size {0}", tempMessages.size());
            messages.addAll(tempMessages);
        });
    }
    
    private Message generateRecord() {
        Date date = new Date(ThreadLocalRandom.current().nextInt() * 1000L);
        String id = UUID.randomUUID().toString();
        String name =  UUID.randomUUID().toString();
        String message = generator.generate(200);
        return new Message(id, name, date, message);
    }
}
