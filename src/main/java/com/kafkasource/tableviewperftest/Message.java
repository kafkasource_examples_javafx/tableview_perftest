/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.kafkasource.tableviewperftest;

import java.util.Date;

/**
 *
 * @author Chad Preisler
 */
public record Message(String id, String name, Date date, String message) {
}
