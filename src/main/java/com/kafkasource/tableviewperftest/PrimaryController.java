package com.kafkasource.tableviewperftest;

//import javafx.collections.transformation.SortedList;
import com.kafkasource.visualkafka.fx.SortedList;
import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import javafx.beans.property.SimpleObjectProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.ListChangeListener;
import javafx.scene.control.Label;

public class PrimaryController {

    @FXML
    private TableColumn<Message, String> dateColumn;

    @FXML
    private TableColumn<Message, String> idColumn;

    @FXML
    private Button loadButton;

    @FXML
    private TableColumn<Message, String> messageColumn;

    @FXML
    private TableColumn<Message, String> nameColumn;

    @FXML
    private TableView<Message> table;
    
    @FXML
    private Label recordCount;
    
    DataLoader loader;
    
    Thread worker;
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS");

    public PrimaryController() {
    }
    
    @FXML
    public void initialize() {
        
        dateColumn.setCellValueFactory(p -> {
            return new SimpleObjectProperty(Instant.ofEpochMilli(p.getValue().date().getTime()).atZone(ZoneId.systemDefault()).toLocalDateTime());
        });
        idColumn.setCellValueFactory(p -> new SimpleStringProperty(p.getValue().id()));
        messageColumn.setCellValueFactory(p -> new SimpleStringProperty(p.getValue().message()));
        nameColumn.setCellValueFactory(p -> new SimpleStringProperty(p.getValue().name()));
        loader = new DataLoader(table.getItems());
        var sortedList =  new SortedList<Message>(table.getItems(), true);
        table.setSortPolicy(sortedList.DEFAULT_SORT_POLICY);
        //var sortedList =  new SortedList<Message>(table.getItems());
        sortedList.comparatorProperty().bind(table.comparatorProperty());
        table.setItems(sortedList);
        table.getSortOrder().add(dateColumn);
        table.sort();
        
        table.getItems().addListener((ListChangeListener<Message>) (change) -> {
            while(change.next()) {
                if (change.wasAdded()) {
                    recordCount.setText(String.valueOf(table.getItems().size()));
                }
            }
        });
    }
    
    @FXML
    void loadData(ActionEvent event) {
        worker = new Thread(loader);
        worker.start();
    }
    
    @FXML
    void stopLoad(ActionEvent event) {
        worker.interrupt();
    }

}
